function addTokens(input, tokens){
    if (typeof input !== 'string') {
        throw "Invalid input";
    }

    if (input.length < 6) {
        throw "Input should have at least 6 characters";
    }

    tokens.forEach(element => {
        if (typeof Object.values(element)[0] !== "string") {
            throw "Invalid array format";
        }
    });

    if(!input.includes("...")) {
        return input;
    }

    let arr = input.split('...')
    let values = [];
    tokens.forEach(element => {
        values.push(Object.values(element));
    });
    let result = '';
    for (let i = 0; i < arr.length - 1; i++) {
        result += arr[i] + "${" + values[i % values.length] + "}";
    }

    result += arr[arr.length - 1];

    return result;

}

const app = {
    addTokens: addTokens
}

module.exports = app;